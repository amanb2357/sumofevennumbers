package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    interface f1{
        public int sumOfEven(int limit) throws IOException;
    }

    public static void main(String[] args) throws IOException {
        f1 obj = (int limit) -> {
            int sum = 0;
            for(int i=0;i<=limit;i+=2) sum+=i;
            return sum;
        };

        System.out.println(obj.sumOfEven(50));
    }
}
